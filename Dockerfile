FROM golang:1.7-alpine
MAINTAINER Marcin Marcinkowski

# Directory for application
ENV APP_HOME /go/src/numbrohome.com/go-banking

# Instal dependencies.
RUN apk --update --upgrade add \
    build-base bash git curl postgresql-client supervisor graphviz ttf-freefont

WORKDIR $APP_HOME
COPY . $APP_HOME

RUN curl https://glide.sh/get | sh \
 && go get -u github.com/mattes/migrate \
 && go get -u github.com/marcinkowski/gin \
 && go get -u github.com/onsi/ginkgo/ginkgo \
 && go get -u github.com/onsi/gomega

CMD $APP_HOME/bin/entrypoint

EXPOSE 80