-- CREATE TABLE "clients" ----------------------------------------
CREATE TABLE "clients" (
	"id" Serial PRIMARY KEY,
	"first_name" Character Varying( 100 ) NOT NULL,
	"last_name" Character Varying( 100 ) NOT NULL,
	"birthday" Date,
	"gender" Character( 1 ),
	"created_at" Timestamp NOT NULL,
	"updated_at" Timestamp,
	"deleted_at" Timestamp
);
-- -------------------------------------------------------------