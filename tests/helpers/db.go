package helpers

import (
	"log"
	"os"
	"os/exec"
)

// MigrateDatabase will run all migrations to create test database.
func MigrateDatabase() {
	_, err := exec.Command(os.Getenv("APP_HOME") + "/bin/migrate").Output()
	if err != nil {
		log.Fatal(err)
	}
}
