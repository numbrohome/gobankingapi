package helpers

import (
	"log"
	"os"
	"os/exec"
)

// PrepareTestDatabase loads all tests fixtures.
func PrepareTestDatabase() {
	_, err := exec.Command("go", "run", os.Getenv("APP_HOME")+"/main.go", "db:fixtures:load").Output()
	if err != nil {
		log.Fatal(err)
	}
}
