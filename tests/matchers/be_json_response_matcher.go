package matchers

import (
	"io"

	"numbrohome.com/go-banking/src/utils"

	"github.com/onsi/gomega/types"

	"fmt"
)

// BeJSON creates new containJSONMatcher with strict size checks.
func BeJSON(expected []byte) types.GomegaMatcher {
	return &containJSONMatcher{
		expected:  expected,
		checkSize: true,
	}
}

// ContainJSON creates new containJSONMatcher
func ContainJSON(expected []byte) types.GomegaMatcher {
	return &containJSONMatcher{
		expected:  expected,
		checkSize: false,
	}
}

type containJSONMatcher struct {
	expected        []byte
	checkSize       bool
	actualDecoded   map[string]interface{}
	expectedDecoded map[string]interface{}
}

func (matcher *containJSONMatcher) Match(actual interface{}) (success bool, err error) {
	body, ok := actual.(io.Reader)
	if !ok {
		return false, fmt.Errorf("ContainJSONResponse matcher expects an io.Reader")
	}

	expectedDecoded, err := utils.DecodeJson(matcher.expected)

	if err != nil {
		return false, fmt.Errorf("Couldn't decode expected json: %s", err.Error())
	}

	actualDecoded, err := utils.DecodeJsonToExpectedType(body, expectedDecoded)

	if err != nil {
		return false, fmt.Errorf("Failed to decode JSON: %s", err.Error())
	}

	matcher.expectedDecoded = expectedDecoded.(map[string]interface{})
	matcher.actualDecoded = actualDecoded.(map[string]interface{})

	return utils.IsMapInMap(matcher.actualDecoded, matcher.expectedDecoded, matcher.checkSize), nil
}

func (matcher *containJSONMatcher) FailureMessage(actual interface{}) (message string) {
	actualDecoded, _ := utils.EncodeFormatedJson(matcher.actualDecoded)
	expectedDecoded, _ := utils.EncodeFormatedJson(matcher.expectedDecoded)
	return fmt.Sprintf(
		"Expected\n%s\nto contain the JSON representation of\n%s",
		string(actualDecoded),
		string(expectedDecoded),
	)
}

func (matcher *containJSONMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	actualDecoded, _ := utils.EncodeFormatedJson(matcher.actualDecoded)
	expectedDecoded, _ := utils.EncodeFormatedJson(matcher.expectedDecoded)
	return fmt.Sprintf(
		"Expected\n%s\nto not contain the JSON representation of\n%s",
		string(actualDecoded),
		string(expectedDecoded),
	)
}
