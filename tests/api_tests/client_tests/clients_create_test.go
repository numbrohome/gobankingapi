package client_tests

import (
	"bytes"
	"net/http"
	"net/http/httptest"

	. "numbrohome.com/go-banking/tests/helpers"
	. "numbrohome.com/go-banking/tests/matchers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Clients Create Endpoint", func() {
	BeforeEach(func() {
		PrepareTestDatabase()
	})

	Context("create a Client Resource", func() {
		It("returns a 201 Status Code and new Client Resource", func() {
			body := bytes.NewReader([]byte(`
			{
				"first_name": "Pablo",
				"last_name": "Escobar",
				"birthday": "1950-08-15T00:00:00.384788Z",
				"gender": "m"
			}`))

			request, _ := http.NewRequest("POST", server.URL+"/v1/clients", body)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)
			expected := []byte(`
			{
				"status": "success",
				"data": {
					"client": {
						"id": 10001,
						"birthday": "1950-08-15T00:00:00.384788Z",
						"gender": "m",
						"first_name": "Pablo",
						"last_name": "Escobar"
 					}
				}
			}`)

			Expect(response.Code).To(Equal(http.StatusCreated))
			Expect(response.Body).To(ContainJSON(expected))
		})
	})
})
