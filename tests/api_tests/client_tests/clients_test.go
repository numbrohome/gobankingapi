package client_tests

import (
	"net/http/httptest"
	"testing"

	"numbrohome.com/go-banking/src/services"
	. "numbrohome.com/go-banking/tests/helpers"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var server *httptest.Server
var router *gin.Engine
var db *gorm.DB

func TestClients(t *testing.T) {
	MigrateDatabase()
	RegisterFailHandler(Fail)
	RunSpecs(t, "Clients Suite")
}

var _ = BeforeSuite(func() {
	container := services.Register()
	db = container.Get("db").(*gorm.DB)
	router = container.Get("router").(*gin.Engine)
	server = httptest.NewServer(router)
})

var _ = AfterSuite(func() {
	server.Listener.Close()
	db.Close()
})
