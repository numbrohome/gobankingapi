package client_tests

import (
	"net/http"
	"net/http/httptest"

	. "numbrohome.com/go-banking/tests/helpers"
	. "numbrohome.com/go-banking/tests/matchers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Clients Index Endpoint", func() {
	BeforeEach(func() {
		PrepareTestDatabase()
	})

	Context("List all the clients", func() {
		It("returns a 200 Status Code and list of clients", func() {
			request, _ := http.NewRequest("GET", server.URL+"/v1/clients", nil)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)
			expected := []byte(`
			{
				"status": "success",
				"data": {
					"clients": [
						{
							"id": 1,
							"first_name": "Marcin",
							"last_name": "Marcinkowski",
							"gender": "m",
							"birthday": "1988-11-10T00:00:00Z",
							"created_at": "2016-01-01T12:30:12Z",
							"updated_at": "2016-01-01T12:30:12Z"
						},
						{"id": 2, "first_name": "Emily", "last_name": "Mebane"},
						{"id": 3, "first_name": "Laura", "last_name": "Calhoun"},
						{"id": 4, "first_name": "John", "last_name": "O'Donnell"},
						{"id": 5, "first_name": "Mark", "last_name": "Turner"},
						{"id": 6, "first_name": "David", "last_name": "Williams"},
						{"id": 7, "first_name": "Ellen", "last_name": "Hudson"}
					]
				}
			}`)

			Expect(response.Code).To(Equal(http.StatusOK))
			Expect(response.Body).To(ContainJSON(expected))
		})
	})
})
