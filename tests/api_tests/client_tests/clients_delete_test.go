package client_tests

import (
	"net/http"
	"net/http/httptest"

	. "numbrohome.com/go-banking/tests/helpers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Clients Delete Endpoint", func() {
	BeforeEach(func() {
		PrepareTestDatabase()
	})

	Context("update a Client Resource", func() {
		It("returns a 204 Status Code and removes Client Resource", func() {
			requestDelete, _ := http.NewRequest("DELETE", server.URL+"/v1/clients/1", nil)
			responseDelete := httptest.NewRecorder()
			router.ServeHTTP(responseDelete, requestDelete)

			requestGet, _ := http.NewRequest("GET", server.URL+"/v1/clients/1", nil)
			responseGet := httptest.NewRecorder()
			router.ServeHTTP(responseGet, requestGet)

			Expect(responseDelete.Code).To(Equal(http.StatusNoContent))
			Expect(responseGet.Code).To(Equal(http.StatusNotFound))
		})
		It("returns a 204 Status Code when client doesn't exist.", func() {
			request, _ := http.NewRequest("DELETE", server.URL+"/v1/clients/9999", nil)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)

			Expect(response.Code).To(Equal(http.StatusNoContent))
		})
	})
})
