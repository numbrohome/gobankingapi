package client_tests

import (
	"bytes"
	"net/http"
	"net/http/httptest"

	. "numbrohome.com/go-banking/tests/helpers"
	. "numbrohome.com/go-banking/tests/matchers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Clients Update Endpoint", func() {
	BeforeEach(func() {
		PrepareTestDatabase()
	})

	Context("update a Client Resource", func() {
		It("returns a 200 Status Code and new Client Resource", func() {
			body := bytes.NewReader([]byte(`
			{
				"first_name": "John",
				"last_name": "Doe",
				"birthday": "1960-08-15T00:00:00.384788Z",
				"gender": "f"
			}`))

			request, _ := http.NewRequest("PATCH", server.URL+"/v1/clients/1", body)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)
			expected := []byte(`
			{
				"status": "success",
				"data": {
					"client": {
						"id": 1,
						"birthday": "1960-08-15T00:00:00.384788Z",
						"gender": "f",
						"first_name": "John",
						"last_name": "Doe"
 					}
				}
			}`)

			Expect(response.Code).To(Equal(http.StatusOK))
			Expect(response.Body).To(ContainJSON(expected))
		})
		It("returns a 404 Status Code when client doesn't exist.", func() {
			body := bytes.NewReader([]byte(`{}`))
			request, _ := http.NewRequest("PATCH", server.URL+"/v1/clients/9999", body)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)

			Expect(response.Code).To(Equal(http.StatusNotFound))
		})
	})
})
