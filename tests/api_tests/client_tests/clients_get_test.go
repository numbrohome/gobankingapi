package client_tests

import (
	"net/http"
	"net/http/httptest"

	. "numbrohome.com/go-banking/tests/helpers"
	. "numbrohome.com/go-banking/tests/matchers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Clients Get Endpoint", func() {
	BeforeEach(func() {
		PrepareTestDatabase()
	})

	Context("Get a single the client", func() {
		It("returns a 200 Status Code and a single client", func() {
			request, _ := http.NewRequest("GET", server.URL+"/v1/clients/1", nil)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)
			expected := []byte(`
			{
				"status": "success",
				"data": {
					"client": {
						"id": 1,
						"first_name": "Marcin",
						"last_name": "Marcinkowski",
						"gender": "m",
						"birthday": "1988-11-10T00:00:00Z",
						"created_at": "2016-01-01T12:30:12Z",
						"updated_at": "2016-01-01T12:30:12Z"
					}
				}
			}`)

			Expect(response.Code).To(Equal(http.StatusOK))
			Expect(response.Body).To(ContainJSON(expected))
		})
		It("returns a 404 Status Code when client doesn't exist.", func() {
			request, _ := http.NewRequest("GET", server.URL+"/v1/clients/9999", nil)
			response := httptest.NewRecorder()
			router.ServeHTTP(response, request)

			Expect(response.Code).To(Equal(http.StatusNotFound))
		})
	})
})