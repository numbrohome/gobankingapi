package controllers

import (
	"log"
	"net/http"

	"numbrohome.com/go-banking/src/models"
	"numbrohome.com/go-banking/src/resources"
	"numbrohome.com/go-banking/src/services/di"
	"numbrohome.com/go-banking/src/services/clients"

	"github.com/gin-gonic/gin"
)

// ClientsController to manage clients resources
type ClientsController struct {
	BaseController
	Clients *clients.Clients
	Logger *log.Logger
}

// NewClientsController creates new struct of ClientsController
func NewClientsController(di *di.DI) *ClientsController {
	return &ClientsController{
		Clients: di.Get("clients").(*clients.Clients),
		Logger: di.Get("logger").(*log.Logger),
	}
}

// Index clients
func (clientsController *ClientsController) Index(context *gin.Context) {
	clients := clientsController.Clients.GetClientCollection(context.Request.URL.Query())
	clientsController.successCollectionResponse(context, clients)
}

// Get one Client
func (clientsController *ClientsController) Get(context *gin.Context) {
	client := clientsController.Clients.GetClientResourceByID(context.Param("id"))

	if client == nil {
		clientsController.notFound(context)
		return
	}

	clientsController.successResourceResponse(context, client, http.StatusOK)
}

// Create Client
func (clientsController *ClientsController) Create(context *gin.Context) {
	var client models.Client

	if clientsController.bind(context, &client) {
		clientResource, validator := clientsController.Clients.CreateClient(&client)

		if validator.HasErrors() {
			clientsController.failure(context, validator.GetErrors(), http.StatusBadRequest)
			return
		}

		clientsController.successResourceResponse(context, clientResource, http.StatusCreated)
	}
}

// Update Client
func (clientsController *ClientsController) Update(context *gin.Context) {
	client := clientsController.Clients.GetClientByID(context.Param("id"))

	if client == nil {
		clientsController.notFound(context)
		return
	}

	if clientsController.bind(context, &client) {
		clientResource, validator := clientsController.Clients.UpdateClient(client)

		if validator.HasErrors() {
			clientsController.failure(context, validator.GetErrors(), http.StatusBadRequest)
			return
		}

		clientsController.successResourceResponse(context, clientResource, http.StatusOK)
	}
}

// Delete a single Client resource
func (clientsController *ClientsController) Delete(context *gin.Context) {
	clientsController.Clients.DeleteClientByID(context.Param("id"))
	clientsController.noContent(context)
}

func (clientsController *ClientsController) successCollectionResponse(context *gin.Context, collection *resources.ClientCollection) {
	clientsController.success(
		context,
		map[string]interface{}{
			"clients": collection.Get(),
		},
		http.StatusOK,
	)
}

func (clientsController *ClientsController) successResourceResponse(context *gin.Context, resource *resources.ClientResource, statusCode int) {
	clientsController.success(
		context,
		map[string]interface{}{
			"client": resource.Get(),
		},
		statusCode,
	)
}
