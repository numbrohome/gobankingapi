package controllers

import (
	"log"
	"net/http"

	"numbrohome.com/go-banking/src/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// BaseController embedded by all other controllers
type BaseController struct {
	DB     *gorm.DB
	Logger *log.Logger
}

func (baseController *BaseController) bind(context *gin.Context, model models.Model) bool {
	err := context.BindJSON(model)

	if err != nil {
		baseController.error(context, err, http.StatusBadRequest)
		return false
	}

	return true
}

func (baseController *BaseController) success(context *gin.Context, data map[string]interface{}, statusCode int) {
	context.JSON(statusCode, gin.H{
		"status": "success",
		"data":   data,
	})
}

func (baseController *BaseController) failure(context *gin.Context, errors interface{}, statusCode int) {
	context.JSON(statusCode, gin.H{
		"status": "fail",
		"data":   errors,
	})
}

func (baseController *BaseController) error(context *gin.Context, message interface{}, statusCode int) {
	context.JSON(statusCode, gin.H{
		"status": "error",
		"data":   message,
	})
}

func (baseController *BaseController) notFound(context *gin.Context) {
	context.JSON(404, gin.H{
		"status": "fail",
		"data": map[string]string{
			"global": "Resource not found.",
		},
	})
}

func (baseController *BaseController) noContent(context *gin.Context) {
	context.JSON(204, gin.H{})
}
