package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

// Transaction Entity
type Transaction struct {
	gorm.Model
	BaseModel

	Principal   string    `json:"principal" binding:"required" gorm:"size:100"`
	Beneficiary string    `json:"beneficiary" binding:"required" gorm:"size:100"`
	Title       string    `json:"title" binding:"required" gorm:"size:200"`
	Reference   string    `json:"reference" binding:"required" gorm:"size:30"`
	IBAN        string    `json:"iban" binding:"required" gorm:"size:30"`
	BIC         string    `json:"bic" binding:"required" gorm:"size:20"`
	Status      string    `json:"status" binding:"required" gorm:"size:30"`
	Type        string    `json:"type" binding:"required" gorm:"size:30"`
	Amount      int64     `json:"amount" binding:"required"`
	Currency    string    `json:"currency" binding:"required" gorm:"size:3"`
	ScheduledAt time.Time `json:"scheduled_at" binding:"required"`
	ExecutedAt  time.Time `json:"executed_at"`

	PrincipalAccount   Account
	PrincipalAccountID uint
	BeneficiaryAccount   Account
	BeneficiaryAccountID uint
}