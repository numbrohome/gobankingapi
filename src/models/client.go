package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

// Client Entity
type Client struct {
	gorm.Model
	BaseModel

	FirstName string     `json:"first_name" binding:"required" gorm:"size:100"`
	LastName  string     `json:"last_name" binding:"required" gorm:"size:100"`
	Gender    string     `json:"gender" gorm:"size:1"`
	Birthday  *time.Time `json:"birthday"`
}