package models

// Model interface
type Model interface{}

// BaseModel embedded in all other models
type BaseModel struct{}
