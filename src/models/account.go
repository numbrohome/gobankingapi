package models

import (
	"github.com/jinzhu/gorm"
)

// Account Entity
type Account struct {
	gorm.Model
	BaseModel

	IBAN     string `json:"iban" binding:"required" gorm:"size:30"`
	Status   string `json:"status" binding:"required" gorm:"size:30"`
	Type     string `json:"type" binding:"required" gorm:"size:30"`
	Currency string `json:"currency" binding:"required" gorm:"size:3"`
	Balance  int64  `json:"balance" binding:"required"`

	Client   Client
	ClientID uint
}