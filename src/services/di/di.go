package di

import (
	"fmt"
	"log"

	"numbrohome.com/go-banking/src/services/logger"
)

// Singleton identifier for single service that can be instantiated only once in the whole application
const Singleton string = "singleton"

// Service struct
type Service struct {
	Callback    func() interface{}
	IsSingleton bool
}

// Call service
func (service *Service) Call() interface{} {
	return service.Callback()
}

// DI - Dependency Injection Service
type DI struct {
	services  map[string]Service
	instances map[string]interface{}
}

// New creates a new dependancy injection container
func New() *DI {
	di := &DI{}
	di.Set("logger", Singleton, func() interface{} {
		return logger.New()
	})

	return di
}

// Get a service from dependency injection container
func (di *DI) Get(serviceName string) interface{} {
	if _, ok := di.services[serviceName]; !ok {
		logger := di.Get("logger").(*log.Logger)
		logger.Println(fmt.Sprintf("Service %s not found", serviceName))

		return nil
	}

	service := di.services[serviceName]

	if service.IsSingleton {
		if di.instances == nil {
			di.instances = make(map[string]interface{})
		}

		if _, ok := di.instances[serviceName]; !ok {
			di.instances[serviceName] = service.Call()
		}

		return di.instances[serviceName]
	}

	return service.Call()
}

// GetCreated returns service only if it's already created
func (di *DI) GetCreated(serviceName string) interface{} {
	if _, ok := di.instances[serviceName]; !ok {
		return nil
	}

	return di.instances[serviceName]
}

// Set new service in DI
func (di *DI) Set(service string, serviceType string, callback func() interface{}) *DI {

	if di.services == nil {
		di.services = map[string]Service{}
	}

	di.services[service] = Service{
		Callback:    callback,
		IsSingleton: (Singleton == serviceType),
	}

	return di
}
