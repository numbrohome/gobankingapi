package di

import (
	"testing"
	"os/exec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestDI(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Dependency Injection Suite")
}

var _ = Describe("Dependency Injection", func() {

	var di *DI

	BeforeEach(func() {
		di = New()
		di.Set("test-service-1", Singleton, func() interface{} {
			return "dummy-test"
		})
		di.Set("test-service-2", Singleton, func() interface{} {
			out, _ := exec.Command("uuidgen").Output()
			return out
		})
	})

	Describe("Get service", func() {
		Context("With given name", func() {
			It("should return right service for given name", func() {
				Expect(di.Get("test-service-1")).To(Equal("dummy-test"))
			})
			It("should return the same instance of the service definied as singleton", func() {
				Expect(di.Get("test-service-2")).To(Equal(di.Get("test-service-2")))
			})
		})
	})
})