package router

import (
	"github.com/gin-gonic/gin"
	"numbrohome.com/go-banking/src/controllers"
	"numbrohome.com/go-banking/src/services/di"
)

func New(di *di.DI) *gin.Engine {
	router := gin.Default()

	v1 := router.Group("/v1")
	{
		clientsController := controllers.NewClientsController(di)

		v1.POST("/clients", clientsController.Create)
		v1.PATCH("/clients/:id", clientsController.Update)
		v1.GET("/clients", clientsController.Index)
		v1.GET("/clients/:id", clientsController.Get)
		v1.DELETE("/clients/:id", clientsController.Delete)
	}

	router.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"company":  "Numbro Home Ltd.",
			"api_name": "GoBankingApi",
		})
	})

	return router
}
