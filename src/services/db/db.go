package db

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
)

// New create a new database connection
func New(logger *log.Logger) *gorm.DB {
	db, err := gorm.Open("postgres", fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_NAME"),
		os.Getenv("DATABASE_PASSWORD"),
	))

	if err != nil {
		logger.Fatal(err)
	}

	db.SetLogger(logger)
	db.LogMode(true)

	return db
}

func CreateDatabase(logger *log.Logger) {
	os.Setenv("PGPASSWORD", os.Getenv("DATABASE_PASSWORD"))
	_, err := exec.Command(
		"psql",
		"-h", os.Getenv("DATABASE_HOST"),
		"-U", os.Getenv("DATABASE_USER"), "-w",
		"-c", fmt.Sprintf("CREATE DATABASE %s", os.Getenv("DATABASE_NAME")),
	).Output()
	os.Unsetenv("PGPASSWORD")

	if err != nil {
		logger.Fatal(err)
	}
}
