package logger

import (
	"log"
	"os"
)

// New creates a new Logger.
func New() *log.Logger {
	file, err := os.OpenFile(os.Getenv("APP_HOME")+"/log/"+os.Getenv("RTE")+".log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

	if err != nil {
		log.Fatalln("Failed to open log file", ":", err)
	}

	return log.New(file, "GREAT_STORAGE: ", log.Ldate|log.Ltime|log.Lshortfile)
}
