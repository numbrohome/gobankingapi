package clients

import (
	"strconv"

	"github.com/jinzhu/gorm"

	"numbrohome.com/go-banking/src/models"
	"numbrohome.com/go-banking/src/resources"
	"numbrohome.com/go-banking/src/validators"
)

// Clients Service
type Clients struct {
	DB *gorm.DB
}

// New creates a new Clients service
func New(db *gorm.DB) *Clients {
	return &Clients{
		DB: db,
	}
}

// GetClientResourceByID returns ClientResource by id.
func (clients *Clients) GetClientResourceByID(id string) *resources.ClientResource {
	client := clients.GetClientByID(id)

	if client == nil {
		return nil
	}

	return resources.NewClientResource(client)
}

// GetClientByID returns Client model by id.
func (clients *Clients) GetClientByID(id string) *models.Client {
	var client models.Client

	if clients.DB.First(&client, id).Error == nil {
		return &client
	}

	return nil
}

// CreateClient creates a new Client resource.
func (clients *Clients) CreateClient(client *models.Client) (*resources.ClientResource, *validators.ClientValidator) {
	validator := validators.NewClientValidator()

	if validator.Validate(client, "client") && clients.DB.Create(&client).Error == nil {
		resource := resources.NewClientResource(client)
		return resource, validator
	}

	return nil, validator
}

// UpdateClient updates a Client resource.
func (clients *Clients) UpdateClient(client *models.Client) (*resources.ClientResource, *validators.ClientValidator) {
	validator := validators.NewClientValidator()

	if validator.Validate(client, "client") && clients.DB.Save(&client).Error == nil {
		resource := resources.NewClientResource(client)
		return resource, validator
	}

	return nil, validator
}

// DeleteClientByID deletes Client by Id.
func (clients *Clients) DeleteClientByID(id string) {
	client := clients.GetClientByID(id)

	if client != nil {
		clients.DB.Delete(&client)
	}
}

// GetClientCollection returns collection of clients.
func (clients *Clients) GetClientCollection(parameters map[string][]string) *resources.ClientCollection {
	var clientsModels []models.Client

	// Find models necessary to build collection.
	db := clients.DB
	db = db.Limit(clients.getLimitFromParameters(parameters))
	db = db.Offset(clients.getOffsetFromParameters(parameters))
	db.Find(&clientsModels)

	// Inject models into ClientCollection.
	clientCollection := resources.NewClientCollection(clientsModels)

	return clientCollection
}

// GetLimit returns a limit for results
func (clients *Clients) getLimitFromParameters(parameters map[string][]string) int {
	limit := 10

	if _, ok := parameters["limit"]; ok {
		if limit, err := strconv.Atoi(parameters["limit"][0]); err == nil {
			return limit
		}
	}

	return limit
}

// GetOffset returns a number for offset.
func (clients *Clients) getOffsetFromParameters(parameters map[string][]string) int {
	offset := 0

	if _, ok := parameters["offset"]; ok {
		if offset, err := strconv.Atoi(parameters["offset"][0]); err == nil {
			return offset
		}
	}

	return offset
}