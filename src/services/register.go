package services

import (
	"log"

	"github.com/jinzhu/gorm"

	"numbrohome.com/go-banking/src/services/db"
	"numbrohome.com/go-banking/src/services/di"
	"numbrohome.com/go-banking/src/services/clients"
	"numbrohome.com/go-banking/src/services/router"
)

// Register all services available by dependency injection
func Register() *di.DI {
	container := di.New()

	container.Set("db", di.Singleton, func() interface{} {
		return db.New(container.Get("logger").(*log.Logger))
	})

	container.Set("router", di.Singleton, func() interface{} {
		return router.New(container)
	})

	container.Set("clients", di.Singleton, func() interface{} {
		return clients.New(container.Get("db").(*gorm.DB))
	})

	return container
}
