package resources

// Resource interface
type Resource interface {
	Get() map[string]interface{}
}

// baseResource embedded in all resources.
type baseResource struct {
	data map[string]interface{}
}
