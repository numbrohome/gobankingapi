package resources

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"numbrohome.com/go-banking/src/models"
)

var _ = Describe("ClientCollection", func() {

	var (
		clients []models.Client
		clientCollection *ClientCollection
	)

	BeforeEach(func() {
		birthday1, _ := time.Parse(time.RFC3339, "2000-11-01T22:08:41+00:00")
		birthday2, _ := time.Parse(time.RFC3339, "1988-11-10T22:08:41+00:00")
		clients = []models.Client{
			{
				FirstName: "DummyFirstName",
				LastName: "DummyLastName",
				Gender: "m",
				Birthday: &birthday1,
			},
			{
				FirstName: "MoreDummyFirstName",
				LastName: "MoreDummyLastName",
				Gender: "k",
				Birthday: &birthday2,
			},
		}

		clientCollection = NewClientCollection(clients)
	})

	Describe("Get data", func() {
		Context("With setted models", func() {
			It("should return an expected collection data", func() {
				Expect(clientCollection.Get()).To(Equal([]map[string]interface{}{
					{
						"id": clients[0].ID,
						"first_name": clients[0].FirstName,
						"last_name": clients[0].LastName,
						"gender": clients[0].Gender,
						"birthday": clients[0].Birthday,
						"created_at": clients[0].CreatedAt,
						"updated_at": clients[0].UpdatedAt,
					},
					{
						"id": clients[1].ID,
						"first_name": clients[1].FirstName,
						"last_name": clients[1].LastName,
						"gender": clients[1].Gender,
						"birthday": clients[1].Birthday,
						"created_at": clients[1].CreatedAt,
						"updated_at": clients[1].UpdatedAt,
					},
				}))
			})
		})
	})
})