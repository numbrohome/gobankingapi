package resources

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"numbrohome.com/go-banking/src/models"
)

var _ = Describe("ClientResource", func() {

	var (
		client *models.Client
		clientResource *ClientResource
	)

	BeforeEach(func() {
		birthday, _ := time.Parse(time.RFC3339, "2000-11-01T22:08:41+00:00")
		client = &models.Client{
			FirstName: "DummyFirstName",
			LastName: "DummyLastName",
			Gender: "m",
			Birthday: &birthday,
		}

		clientResource = NewClientResource(client)
	})

	Describe("Get data", func() {
		Context("With setted model", func() {
			It("should return an expected resource data", func() {
				Expect(clientResource.Get()).To(Equal(map[string]interface{}{
					"id": client.ID,
					"first_name": client.FirstName,
					"last_name": client.LastName,
					"gender": client.Gender,
					"birthday": client.Birthday,
					"created_at": client.CreatedAt,
					"updated_at": client.UpdatedAt,
				}))
			})
		})
	})
})
