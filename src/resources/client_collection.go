package resources

import (
	"numbrohome.com/go-banking/src/models"
)

// ClientCollection for Client data
type ClientCollection struct {
	baseCollection
}

// NewClientCollection creates new collection of ClientCollection.
func NewClientCollection(clients []models.Client) *ClientCollection {
	clientCollection := ClientCollection{}

	for index := range clients {
		clientCollection.AddResource(NewClientResource(&clients[index]))
	}

	return &clientCollection
}