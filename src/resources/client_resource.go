package resources

import (
	"numbrohome.com/go-banking/src/models"
)

// ClientResource represents a resource of one client
type ClientResource struct {
	baseResource
	client *models.Client
}

// NewClientResource creates a new resource of Client.
func NewClientResource(client *models.Client) *ClientResource {
	clientResource := ClientResource{}
	clientResource.client = client

	return &clientResource
}

// Get resource representation.
func (clientResource *ClientResource) Get() map[string]interface{} {
	return map[string]interface{} {
		"id": clientResource.client.ID,
		"first_name": clientResource.client.FirstName,
		"last_name": clientResource.client.LastName,
		"gender": clientResource.client.Gender,
		"birthday": clientResource.client.Birthday,
		"created_at": clientResource.client.CreatedAt,
		"updated_at": clientResource.client.UpdatedAt,
	}
}