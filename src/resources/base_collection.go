package resources

type Collection interface {
	AddResource(resource Resource)
	GetResources() []Resource
	Get() []map[string]interface{}
}

type baseCollection struct {
	resources []Resource
	data	  []map[string]interface{}
}

// AddResource adds an new resource to collection.
func (baseCollection *baseCollection) AddResource(resource Resource) {
	baseCollection.resources = append(baseCollection.resources, resource)
}

// GetResources gets all resources from collection.
func (baseCollection *baseCollection) GetResources() []Resource {
	return baseCollection.resources
}

// Get returns collection's data.
func (baseCollection *baseCollection) Get() []map[string]interface{} {
	if len(baseCollection.data) == 0 {
		baseCollection.data = make([]map[string]interface{}, len(baseCollection.resources))
		for index := range baseCollection.resources {
			baseCollection.data[index] = baseCollection.resources[index].Get()
		}
	}

	return baseCollection.data
}