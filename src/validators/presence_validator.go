package validators

import (
	"fmt"
	"numbrohome.com/go-banking/src/utils"
)

// PresenceValidator validates that the specified attribute is not empty.
type PresenceValidator struct {
	baseValidator
	Message string
}

// NewPresenceValidator creates a new PresenceValidator.
func NewPresenceValidator() *PresenceValidator {
	return &PresenceValidator{
		Message: "Field %s can't be empty.",
	}
}

// Validate validates that the specified attribute is not empty.
func (presenceValidator *PresenceValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {

	if utils.IsEmpty(obj) {
		presenceValidator.AddError(name, fmt.Sprintf(presenceValidator.Message, name))
		return false
	}

	return true
}