package validators

import (
	"numbrohome.com/go-banking/src/models"
)

// ClientValidator validates Client model.
type ClientValidator struct {
	baseValidator
}

// NewClientValidator creates a new struct of ClientValidator.
func NewClientValidator() *ClientValidator {
	return &ClientValidator{}
}

// Validate validates Client model
func (clientValidator *ClientValidator) Validate(obj interface{},  name string, parameters ...interface{}) bool {
	client, ok := obj.(*models.Client)

	if !ok {
		clientValidator.AddError("global", "Unknown type for ClientValidator.")
		return false
	}

	presenceValidator := NewPresenceValidator()
	presenceValidator.Validate(client.FirstName, "first_name")
	presenceValidator.Validate(client.LastName, "last_name")
	presenceValidator.Validate(client.Gender, "gender")
	presenceValidator.Validate(client.Birthday, "birthday")

	lengthValidator := NewLengthValidator()
	lengthValidator.Validate(client.FirstName, "first_name", "2", "100")
	lengthValidator.Validate(client.LastName, "last_name", "2", "100")

	inclusionValidator := NewInclusionValidator()
	inclusionValidator.Validate(client.Gender, "gender", []string{"m", "f"})

	clientValidator.AddErrors(presenceValidator.GetErrors())
	clientValidator.AddErrors(lengthValidator.GetErrors())
	clientValidator.AddErrors(inclusionValidator.GetErrors())

	return !clientValidator.HasErrors()
}