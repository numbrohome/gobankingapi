package validators

import (
	"fmt"
	"strconv"
	"numbrohome.com/go-banking/src/utils"
)

// IntegerValidator validates that value is an integer.
type IntegerValidator struct {
	baseValidator
	Message string
}

// NewIntegerValidator creates a new IntegerValidator.
func NewIntegerValidator() *IntegerValidator {
	return &IntegerValidator{
		Message: "Field %s has to be an integer number.",
	}
}

// Validate validates that value is an integer.
func (integerValidator *IntegerValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {

	if obj == nil {
		return true
	}

	_, err := strconv.Atoi(utils.ToString(obj))

	if err != nil {
		integerValidator.AddError(name, fmt.Sprintf(integerValidator.Message, name))
		return false
	}

	return true
}