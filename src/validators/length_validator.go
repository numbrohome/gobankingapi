package validators

import (
	"fmt"

	"unicode/utf8"
	"numbrohome.com/go-banking/src/utils"
)

// LengthValidator validates the length of the string.
type LengthValidator struct {
	baseValidator
	MessageEqual string
	MessageMax   string
	MessageMin   string
}

// NewLengthValidator creates a new LengthValidator.
func NewLengthValidator() *LengthValidator {
	return &LengthValidator{
		MessageEqual: "The length of %s has to be equal %d characters",
		MessageMax:   "The length of %s cannot exceed %d characters",
		MessageMin:   "The length of %s cannot be less than %d characters",
	}
}

// Validate validates the length of string.
func (lengthValidator *LengthValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {

	if obj == nil {
		return true
	}

	strLength := utf8.RuneCountInString(obj.(string))

	switch len(parameters) {
	case 2:
		min := utils.ToInt(parameters[0])
		max := utils.ToInt(parameters[1])

		if strLength < int(min) {
			lengthValidator.AddError(name, fmt.Sprintf(lengthValidator.MessageMin, name, min))
			return false
		}

		if strLength > int(max) {
			lengthValidator.AddError(name, fmt.Sprintf(lengthValidator.MessageMax, name, max))
			return false
		}

	case 1:
		length := utils.ToInt(parameters[0])
		if strLength != int(length) {
			lengthValidator.AddError(name, fmt.Sprintf(lengthValidator.MessageEqual, name, length))
			return false
		}
	default:
		panic("Wrong number of paramerters.")
	}

	return true
}
