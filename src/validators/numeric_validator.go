package validators

import (
	"fmt"
	"strconv"
	"numbrohome.com/go-banking/src/utils"
)

// NumericValidator validates that value is numeric.
type NumericValidator struct {
	baseValidator
	Message string
}

// NewNumericValidator creates a new NumericValidator.
func NewNumericValidator() *NumericValidator {
	return &NumericValidator{
		Message: "Field %s has to be a number.",
	}
}

// Validate validates that value is numeric.
func (numericValidator *NumericValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {

	if obj == nil {
		return true
	}

	_, err := strconv.ParseFloat(utils.ToString(obj), 64)

	if err != nil {
		numericValidator.AddError(name, fmt.Sprintf(numericValidator.Message, name))
		return false
	}

	return true
}