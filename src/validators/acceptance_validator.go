package validators

import (
	"fmt"
)

// AcceptanceValidator validates that a field is accepted.
type AcceptanceValidator struct {
	baseValidator
	Message string
}

// NewAcceptanceValidator creates a new AcceptanceValidator.
func NewAcceptanceValidator() *AcceptanceValidator {
	return &AcceptanceValidator{
		Message: "Field %s has to be accepted.",
	}
}

// Validate validates that the specified attribute is not empty.
func (acceptanceValidator *AcceptanceValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {
	if obj == nil || obj == "" || obj == "false" || obj == 0 || obj == "0" {
		acceptanceValidator.AddError(name, fmt.Sprintf(acceptanceValidator.Message, name))
		return false
	}

	return true
}