package validators

import (
	"fmt"

	"numbrohome.com/go-banking/src/utils"
)

// InclusionValidator validates that value is included in a given set.
type InclusionValidator struct {
	baseValidator
	Message string
}

// NewInclusionValidator creates a new InclusionValidator.
func NewInclusionValidator() *InclusionValidator {
	return &InclusionValidator{
		Message: "The defined set for field %s does not contain value %s.",
	}
}

// Validate validates that value is included in a given set.
func (inclusionValidator *InclusionValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {
	if len(parameters) != 1 {
		panic("Wrong number of paramerters.")
	}

	search := utils.ToString(obj)
	set, ok := parameters[0].([]string)

	if !ok {
		panic("The set for InclusionValidator has to be a slice of strings.")
	}

	if obj == nil {
		return true
	}

	if !utils.InStringSlice(set, search) {
		inclusionValidator.AddError(name, fmt.Sprintf(inclusionValidator.Message, name, search))
		return false
	}

	return true
}