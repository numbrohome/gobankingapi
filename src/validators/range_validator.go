package validators

import (
	"fmt"
	"numbrohome.com/go-banking/src/utils"
)

type RangeValidator struct {
	baseValidator
	Message string
}

// NewRangeValidator creates a new RangeValidator.
func NewRangeValidator() *RangeValidator {
	return &RangeValidator{
		Message: "The value of %s has to be between %g and %g, %g given.",
	}
}

// Validate validates a number in range.
func (rangeValidator *RangeValidator) Validate(obj interface{}, name string, parameters ...interface{}) bool {

	if obj == nil || !utils.IsNumeric(obj) {
		return true
	}

	if len(parameters) != 2 {
		panic("Wrong number of paramerters.")
	}

	number := utils.ToFloat(obj)
	min := utils.ToFloat(parameters[0])
	max := utils.ToFloat(parameters[1])

	if number < min || number > max {
		rangeValidator.AddError(name, fmt.Sprintf(rangeValidator.Message, name, min, max, number))
		return false
	}

	return true
}