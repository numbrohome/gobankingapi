package validators

import "reflect"

// Validator to validate different objects
type Validator interface {
	Validate(obj interface{}, name string, parameters ...interface{}) bool
	AddError(field string, error interface{})
	AddErrors(errors *map[string][]interface{})
	HasErrors() bool
	GetErrors() *map[string][]interface{}
}

// baseValidator embedded in all Validators
type baseValidator struct {
	validators []Validator
	errors     map[string][]interface{}
}

// AddError adds a new error message to the object
func (baseValidator *baseValidator) AddError(field string, error interface{}) {
	if len(baseValidator.errors) == 0 {
		baseValidator.errors = map[string][]interface{}{}
	}

	if _, ok := baseValidator.errors[field]; !ok {
		baseValidator.errors[field] = []interface{}{}
	}

	baseValidator.errors[field] = append(baseValidator.errors[field], error)
}

// HasErrors returns true if object has any error
func (baseValidator *baseValidator) HasErrors() bool {
	for field := range baseValidator.errors {
		for index := range baseValidator.errors[field] {
			value := baseValidator.errors[field][index]
			kind := reflect.TypeOf(value).Kind()

			if kind == reflect.Map && len(value.(map[string][]interface{})) == 0 {
				continue
			}

			return true;
		}
	}

	return false
}

// GetErrors returns all errors
func (baseValidator *baseValidator) GetErrors() map[string][]interface{} {
	return baseValidator.errors
}

// AddErrors adds group of errors
func (baseValidator *baseValidator) AddErrors(errors map[string][]interface{}) {
	if len(baseValidator.errors) == 0 {
		baseValidator.errors = map[string][]interface{}{}
	}

	for field, fieldErrors := range errors {
		if _, ok := baseValidator.errors[field]; !ok {
			baseValidator.errors[field] = []interface{}{}
		}

		baseValidator.errors[field] = append(baseValidator.errors[field], fieldErrors...)
	}
}