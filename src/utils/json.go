package utils

import (
	"encoding/json"
	"io"
	"reflect"
)

func DecodeJsonToExpectedType(jsonString io.Reader, expected interface{}) (interface{}, error) {
	pointerToObjectOfExpectedType := reflect.New(reflect.TypeOf(expected)).Interface()
	err := json.NewDecoder(jsonString).Decode(pointerToObjectOfExpectedType)

	if err != nil {
		return nil, err
	}

	return reflect.ValueOf(pointerToObjectOfExpectedType).Elem().Interface(), err
}

func DecodeJson(jsonString []byte) (interface{}, error) {
	var object interface{}
	err := json.Unmarshal(jsonString, &object)
	if err != nil {
		return nil, err
	}
	return object, err
}

func EncodeFormatedJson(data interface{}) ([]byte, error) {
	jsonString, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		return nil, err
	}
	return jsonString, err
}
