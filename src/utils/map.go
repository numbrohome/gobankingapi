package utils

import "reflect"

// IntersectMapStringKeys computes the intersection of map keys.
func IntersectMapStringKeys(base map[string]interface{}, keys ...[]string) map[string]interface{} {
	newMap := map[string]interface{}{}
	for _, filterKeys := range keys {
		for _, key := range filterKeys {
			if value, ok := base[key]; ok {
				newMap[key] = value
			}
		}
	}

	return newMap
}

// GetMapStringKeys returns a slice made of map's keys.
func GetMapStringKeys(base map[string]string) []string {
	keys := make([]string, len(base))
	for k := range base {
		keys = append(keys, k)
	}

	return keys
}

// IsMapInMap checks if one map is in another.
func IsMapInMap(base, search map[string]interface{}, lengthCheck bool) bool {
	if lengthCheck && len(base) != len(search) {
		return false
	}

	for key, value := range search {
		baseValue, ok := base[key]
		if !ok {
			return false
		}

		if value == nil || baseValue == nil {
			if value != baseValue {
				return false
			}
			continue
		}

		kind := reflect.TypeOf(value).Kind()

		if kind != reflect.TypeOf(baseValue).Kind() {
			return false
		}

		if kind == reflect.Map {
			if !IsMapInMap(baseValue.(map[string]interface{}), value.(map[string]interface{}), lengthCheck) {
				return false
			}
			continue
		}

		if kind == reflect.Slice {
			if !IsSliceInSlice(baseValue.([]interface{}), value.([]interface{}), lengthCheck) {
				return false
			}
			continue
		}

		if baseValue != value {
			return false
		}
	}

	return true
}
