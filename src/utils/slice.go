package utils

import "reflect"

// InStringSlice checks if a value exists in a slice.
func InStringSlice(slice []string, search string) bool {
	for _, value := range slice {
		if value == search {
			return true
		}
	}
	return false
}

// IntersectStringSlice computes the intersection of slices.
func IntersectStringSlice(slice []string, slices ...[]string) []string {
	newSlice := []string{}
	for _, filterSlice := range slices {
		for i := len(filterSlice) - 1; i >= 0; i-- {
			element := filterSlice[i]
			if InStringSlice(slice, element) {
				newSlice = append(newSlice, filterSlice[i])
			}
		}
	}

	return newSlice
}

// IsSliceInSlice checks if one slice is in another.
func IsSliceInSlice(base, search []interface{}, lengthCheck bool) bool {
	baseLength := len(base)
	if lengthCheck && baseLength != len(search) {
		return false
	}

	for index, value := range search {
		var baseValue interface{}
		if baseLength-1 >= index {
			baseValue = base[index]
		} else {
			return false
		}

		if value == nil || baseValue == nil {
			if value != baseValue {
				return false
			}
			continue
		}

		kind := reflect.TypeOf(value).Kind()

		if kind != reflect.TypeOf(baseValue).Kind() {
			return false
		}

		if kind == reflect.Slice {
			if !IsSliceInSlice(baseValue.([]interface{}), value.([]interface{}), lengthCheck) {
				return false
			}
			continue
		}

		if kind == reflect.Map {
			if !IsMapInMap(baseValue.(map[string]interface{}), value.(map[string]interface{}), lengthCheck) {
				return false
			}
			continue
		}

		if baseValue != value {
			return false
		}
	}

	return true
}
