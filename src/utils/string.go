package utils

import (
	"fmt"
	"reflect"
)

func ToString(x interface{}) string {
	return fmt.Sprintf("%v", x)
}

func IsEmpty(x interface{}) bool {
	return x == nil || x == reflect.Zero(reflect.TypeOf(x)).Interface()
}