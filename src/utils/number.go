package utils

import (
	"fmt"
	"strconv"
)

func ToInt(x interface{}) int {
	value, err := strconv.Atoi(ToString(x))

	if err != nil {
		panic(fmt.Sprintf("Couldn't convert value %v to inteager.", x))
	}

	return value
}

func ToFloat(x interface{}) float64 {
	value, err := strconv.ParseFloat(ToString(x), 64)

	if err != nil {
		panic(fmt.Sprintf("Couldn't convert value %v to float.", value))
	}

	return value
}

// Check if value is numeric.
func IsNumeric(x interface{}) bool {
	_, err := strconv.ParseFloat(ToString(x), 64)

	return err == nil
}