# Go Banking

It's a code sample of API for banking application made in Golang.

Created By: Numbro Home Ltd.

## Development Setup
```
$ docker-compose run --rm go_banking_development bin/build
$ docker-compose run --rm go_banking_development bin/migrate
$ docker-compose up -d go_banking_development
$ docker-compose exec go_banking_development build/go-banking db:fixtures:load
```
## Test Setup
```
$ docker-compose run --rm go_banking_test bin/build
$ docker-compose up -d go_banking_test
$ docker-compose exec go_banking_test build/go-banking db:create
```
## Commands
```
db:create            Create database.
db:fixtures:load     Load all database fixtures.
server:start         Start application server.
```

## API

### Clients Endpoints
| HTTP Method | Endpoint        | Request Body                                                                                         |
|-------------|-----------------|------------------------------------------------------------------------------------------------------|
| GET         | /v1/clients     | N/A                                                                                                  |
| GET         | /v1/clients/:id | N/A                                                                                                  |
| POST        | /v1/clients     | {"first_name": "John", "last_name": "Doe", "birthday": "1950-08-15T00:00:00.384788Z", "gender": "m"} |
| DELETE      | /v1/clients/:id |                                                                                                      |
| PATCH       | /v1/clients/:id | {"first_name": "John", "last_name": "Doe", "birthday": "1950-08-15T00:00:00.384788Z", "gender": "m"} |

### [TODO] Accounts Endpoints
| HTTP Method | Endpoint                                    | Request Body                                                                                                                  |
|-------------|---------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| GET         | /v1/clients/:client_id/accounts             | N/A                                                                                                                           |
| GET         | /v1/clients/:client_id/accounts/:account_id | N/A                                                                                                                           |
| POST        | /v1/clients/:client_id/accounts             | {"iban": "GB29NWBK60161331926819", "status": "active", "type": "basic", "currency": "GBP", "balance": 10000} |
| PATCH       | /v1/clients/:client_id/accounts/:account_id | {"status": "blocked"}                                                                                                         |

### [TODO] Transactions Endpoints
| HTTP Method | Endpoint                              | Request Body                                                                                                                                                                                                                                                                            |
|-------------|---------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GET         | /v1/accounts/:account_id/transactions | N/A                                                                                                                                                                                                                                                                                     |
| POST        | /v1/accounts/:account_id/transactions | {"principal": "Principal Name", "beneficiary": "Beneficiary Name", "title": "Test", "reference": "TESTPAYMENT",,"iban": "DE89370400440532013000", "bic": "DBXXXXX", "status": "new", "type":"SWIFT", "currency": "GBP", "amount": 32999, "scheduled_at": "2017-08-15T00:00:00.384788Z"} |