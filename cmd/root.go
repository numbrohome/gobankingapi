package cmd

import (
	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{
	Use:   "go-banking",
	Short: "An API for simple banking application.",
	Long:  `It's a code sample of API for bank application.`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}