package cmd

import (
	"numbrohome.com/go-banking/src/services/db"
	"numbrohome.com/go-banking/src/services/logger"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(databaseCreateCmd)
}

var databaseCreateCmd = &cobra.Command{
	Use:   "db:create",
	Short: "Create database.",
	Long:  `Create database.`,
	Run: func(cmd *cobra.Command, args []string) {
		db.CreateDatabase(logger.New())
	},
}