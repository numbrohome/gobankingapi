package cmd

import (
	"fmt"
	"os"

	"numbrohome.com/go-banking/src/services"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(serverCmd)
}

var serverCmd = &cobra.Command{
	Use:   "server:start",
	Short: "Start application server.",
	Long:  `Start application server giving access to API.`,
	Run: func(cmd *cobra.Command, args []string) {
		engine, db := getMainEngine()
		engine.Run(fmt.Sprintf(":%s", os.Getenv("PORT")))
		db.Close()
	},
}

// GetMainEngine returns router engine and db connection.
func getMainEngine() (*gin.Engine, *gorm.DB) {
	container := services.Register()
	return container.Get("router").(*gin.Engine), container.Get("db").(*gorm.DB)
}