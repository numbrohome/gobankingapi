package cmd

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
	"github.com/spf13/cobra"
	"gopkg.in/testfixtures.v1"
)

func init() {
	RootCmd.AddCommand(FixturesLoadCmd)
}

var FixturesLoadCmd = &cobra.Command{
	Use:   "db:fixtures:load",
	Short: "Load all database fixtures.",
	Long:  `Load all database fixtures for test database.`,
	Run: func(cmd *cobra.Command, args []string) {
		db := getDatabaseConnection()

		if os.Getenv("RTE") != "production" {
			testfixtures.SkipDatabaseNameCheck(true)
		}

		err := testfixtures.LoadFixtures(os.Getenv("APP_HOME")+"/db/fixtures", db, &testfixtures.PostgreSQLHelper{})
		if err != nil {
			log.Fatal(err)
		}
	},
}

// GetDatabaseConnection gives access to database.
func getDatabaseConnection() *sql.DB {
	db, err := sql.Open("postgres", fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_NAME"),
		os.Getenv("DATABASE_PASSWORD"),
	))

	if err != nil {
		log.Fatal(err)
	}

	return db
}