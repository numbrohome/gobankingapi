package main

import (
	"fmt"
	"os"

	"numbrohome.com/go-banking/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}